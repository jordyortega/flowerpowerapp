﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlowerPower.Models;
using Microsoft.AspNet.Identity;

namespace FlowerPower.Controllers
{
    public class ConfirmationController : Controller
    {

        DB_A3D6D6_FlowerPowerJordyEntities db = new DB_A3D6D6_FlowerPowerJordyEntities();

        // GET: Confirmation
        public ActionResult Index()
        {
            return View(CartViewModel.shoppingCartList);
        }

        [Authorize(Roles = "Admin,Klant,Medewerker")]
        public ActionResult ChooseStore()
        {
            IEnumerable<Store> stores = db.Store;
            SelectList storeList = new SelectList(stores, "storeID", "address");

            storeList.OrderBy(l => l.Value);

            ViewBag.Stores = storeList;

            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Klant,Medewerker")]
        public ActionResult GetOrderButton(int? storeID)
        {
            if (storeID == null)
            {
                return new HttpNotFoundResult();
            }

            return PartialView("GetOrderButtonPartial");
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Klant,Medewerker")]
        public ActionResult OnClickStoreButton(int? storeID)
        {
            bool[] inList = new bool[999];
            int[] productQuantity = new int[999];
            int[] productID = new int[999];
            int i = 0;

            if (storeID == null)
            {
                return new HttpNotFoundResult();
            }

            Order order = new Order
            {
                guestID = User.Identity.GetUserId(),
                storeID = storeID
            };

            db.Order.Add(order);

            foreach (Product p in CartViewModel.shoppingCartList)
            {
                if (!inList[p.productID])
                {
                    inList[p.productID] = true;
                    productQuantity[p.productID] = 1;
                    productID[i] = p.productID;
                }
                else
                {
                    productQuantity[p.productID]++;
                }
                i++;
            }

            foreach (var item in productID)
            {
                if (item == 0)
                {
                    continue;
                }

                OrderRule o = new OrderRule
                {
                    orderID = order.orderID,
                    quantity = productQuantity[item],
                    productID = item
                };

                db.OrderRule.Add(o);
            }

            db.SaveChanges();

            return RedirectToAction("Index", "PDF", null);
        }

        [Authorize(Roles = "Admin,Klant,Medewerker")]
        public ActionResult Succes()
        {

            return View("~/Views/PDF/Index");
        }
    }
}