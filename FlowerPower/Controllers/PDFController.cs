﻿using FlowerPower.Models;
using Rotativa;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace FlowerPower.Controllers
{ 
    
    public class PDFController : Controller
    {
         DB_A3D6D6_FlowerPowerJordyEntities db = new DB_A3D6D6_FlowerPowerJordyEntities();
         public ActionResult PrintIndex()
         {
            CartViewModel.savedUserID = User.Identity.GetUserId();
            return new ActionAsPdf("Index") { FileName = "Factuur.pdf" };
         }

        public ActionResult Index()
        {
            string userID = "";
            if (CartViewModel.savedUserID != "")
            {
                userID = CartViewModel.savedUserID;
            }
            else
            {
                userID = User.Identity.GetUserId();
            }
            Order order = db.Order.OrderByDescending(o => o.orderID).Where(o => o.guestID == userID).First();

            ViewBag.Message = string.Format($"Hello {0} ASP.NET MVC!");

            return View(order);
        }

        public ActionResult TestViewWithModel(string id)
        {
            var model = db.Order;
            return new ViewAsPdf(model);
        }
    }
}