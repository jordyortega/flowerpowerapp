﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FlowerPower.Models;

namespace FlowerPower.Controllers
{
    [Authorize(Roles = "Admin")]
    public class MedewerkerController : Controller
    {

        private DB_A3D6D6_FlowerPowerJordyEntities db = new DB_A3D6D6_FlowerPowerJordyEntities();

        // GET: Medewerker
        public ActionResult Index()
        {
            var userlist = db.AspNetUsers.OrderBy(u => u.UserName).ToList().Select(uu =>
             new SelectListItem {
                 Value = uu.Id.ToString(),
                 Text = uu.UserName }).ToList();
            ViewBag.Users = userlist;

            var employee = db.Employee.Include(e => e.Store);
            return View(employee.ToList());
        }

        // GET: Medewerker/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employee.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: Medewerker/Create
        public ActionResult Create()
        {
            ViewBag.storeID = new SelectList(db.Store, "storeID", "storeName");
            return View();
        }

        // POST: Medewerker/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "employeeID,employeeFirstName,employeeLastName,storeID")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Employee.Add(employee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.storeID = new SelectList(db.Store, "storeID", "storeName", employee.storeID);
            return View(employee);
        }

        // GET: Medewerker/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employee.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.storeID = new SelectList(db.Store, "storeID", "storeName", employee.storeID);
            return View(employee);
        }

        // POST: Medewerker/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "employeeID,employeeFirstName,employeeLastName,storeID")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.storeID = new SelectList(db.Store, "storeID", "storeName", employee.storeID);
            return View(employee);
        }

        // GET: Medewerker/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employee.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Medewerker/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee employee = db.Employee.Find(id);
            db.Employee.Remove(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
