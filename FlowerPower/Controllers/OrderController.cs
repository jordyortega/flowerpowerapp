﻿using FlowerPower.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlowerPower;
using System.Net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace FlowerPower.Controllers
{
    [Authorize(Roles = "Medewerker, Admin")]
    public class OrderController : Controller
    {
        public DB_A3D6D6_FlowerPowerJordyEntities db = new DB_A3D6D6_FlowerPowerJordyEntities();

        // GET: Order
        public ActionResult Index()
        {
            var orderList = db.Order.ToList();

            if(!User.IsInRole("Admin"))
            {
                string userID = User.Identity.GetUserId();
                Employee employee = db.Employee.Where(e => e.employeeID == userID).First();
                if (employee != null)
                {
                    orderList = db.Order.Where(o => o.storeID == employee.storeID).ToList();
                }
            }

            return View(orderList);
        }

        public ActionResult Cancel(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Order order = db.Order.Find(id);

            order.cancelled = true;

            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult FinishOrder(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Order order = db.Order.Find(id);
            order.finished = true;
            db.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}