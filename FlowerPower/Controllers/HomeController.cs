﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FlowerPower.Models;

namespace FlowerPower.Controllers
{
    [RequireHttps]
    public class HomeController : Controller
    {
        private DB_A3D6D6_FlowerPowerJordyEntities db = new DB_A3D6D6_FlowerPowerJordyEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

    }
}