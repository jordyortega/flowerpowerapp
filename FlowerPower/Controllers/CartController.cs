﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using FlowerPower.Models;

namespace FlowerPower.Controllers
{
    public class CartController : Controller
    {
        DB_A3D6D6_FlowerPowerJordyEntities db = new DB_A3D6D6_FlowerPowerJordyEntities();

        private List<Product> shoppingCartList = CartViewModel.shoppingCartList;

        // GET: Cart
        public ActionResult Index()
        {
            shoppingCartList.OrderByDescending(s => s.productPrice);

            return View(shoppingCartList);
        }

        [HttpPost]
        public ActionResult AddToCart(int productID)
        {
            Product p = db.Product.Where(m => m.productID == productID).First();

            if(!CartViewModel.shoppingCartList.Contains(p)) //if product is not in list, add it
            {
                CartViewModel.shoppingCartList.Add(p);
            }

            return PartialView("CartPartial", CartViewModel.shoppingCartList);
        }

        [HttpPost]
        public ActionResult RemoveFromCart(int productID)
        {
            List<Product> products = CartViewModel.shoppingCartList.Where(m => m.productID == productID).ToList();

            foreach (Product product in products)
            {
                if (product.productID == productID)
                {
                    CartViewModel.shoppingCartList.Remove(product);
                    break;
                }
            }

            return PartialView("CartPartial", CartViewModel.shoppingCartList);
        }

        [HttpPost]
        public ActionResult RemoveAllFromCart(int productID)
        {
            List<Product> products = CartViewModel.shoppingCartList.Where(m => m.productID == productID).ToList();

            foreach(Product product in products)
            {
                if(product.productID == productID)
                {
                    CartViewModel.shoppingCartList.Remove(product);
                }
            }

            return PartialView("CartPartial", CartViewModel.shoppingCartList);
        }

        [HttpPost]
        public ActionResult ShowCart()
        {

            return PartialView("CartPartial", CartViewModel.shoppingCartList);
        }
    }
}